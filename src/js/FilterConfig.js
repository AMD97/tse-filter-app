export const filterConfigDefault = {
    id: 1, //id 1 reserved for default filter
    title: "فیلتر پیشفرض",
    template: [
        {field: 0, active: true, title: "نماد",},
        {field: 1, active: true, title: "حجم",},
        {field: 2, active: true, title: "ق. بازگشایی",},
        {field: 3, active: true, title: "درصد ق. بازگشایی",},
        {field: 4, active: true, title: "ق. آخر",},
        {field: 5, active: true, title: "درصد ق. آخر",},
        {field: 6, active: true, title: "ق. پایانی",},
        {field: 7, active: true, title: "درصد ق. پایانی",},
        {field: 8, active: false, title: "",},
        {field: 9, active: false, title: "",},
        {field: 10, active: false, title: "",},
        {field: 11, active: false, title: "",},
        {field: 12, active: false, title: "",},
        {field: 13, active: false, title: "",},
        {field: 14, active: false, title: "",},
        {field: 15, active: false, title: "",},
    ],
    industry: {
        all: true,
        selected: []
    },
    asset: {
        stockExchange: true, // سهام بورس
        stockOTC: true, //سهام فرابورس
        basisOTC: true, // سهام پایه فرابورس
        priority: false, //حق تقدم
        fund: true, // صندوق
        debtSecurity: false, //اوراق بدهی
        option: false, // اختیار معامله
        commodity: false, // بورس کالا
        future: false, // آتی
        housingBonds: false, // اوراق تسهیلات مسکن
    },
    intervalTime: 10,
    sortProperty:
        `industry:صنعت
symbol:نماد`,
    css: '',

    code: ` 
 
function _init() {
}

function _compute() {
}

function _filter() {
    return true;
}

function _fields() {
    return {
        0: _symbol,
        1: _tradeVolume,
        2: _priceOpen,
        3: _priceOpenP,
        4: _priceClose,
        5: _priceCloseP,
        6: _priceFinal,
        7: _priceFinalP,
    }
    
}

const _sortProperty = {
    industry: _industryCode,
    symbol: _symbol,
}

            `
};

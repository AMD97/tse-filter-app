
export const round = (number, decimal) => {
    return Math.round(number * Math.pow(10, decimal)) / Math.pow(10, decimal)
}

export const dateToGregorian = (dateStr) => {
    return moment.from(dateStr, 'fa', 'YYYY/MM/DD').format('YYYYMMDD');
}

import {round} from './Utility.js'

const rowModel = {
    id: null,
    id2: null,
    symbol: null,
    name: null,
    priceYesterday: null,
    baseVolume: null,
    visitCount: null,
    flow: null,
    industryCode: null,
    priceMax: null,
    priceMaxC: function (day) {
        return this.priceMax[day] - this.priceYesterday[day]
    },
    priceMaxP: function (day) {
        return round(100 * ((this.priceMax[day] - this.priceYesterday[day]) / this.priceYesterday[day]), 2)
    },
    priceMin: null,
    priceMinC: function (day) {
        return this.priceMin[day] - this.priceYesterday[day]
    },
    priceMinP: function (day) {
        return round(100 * ((this.priceMin[day] - this.priceYesterday[day]) / this.priceYesterday[day]), 2)
    },
    stockCount: null,
    yval: null,
    heven: null,
    dayIndexByDate: null,
    priceOpen: null,
    priceOpenC: function (day) {
        return this.priceOpen[day] - this.priceYesterday[day]
    },
    priceOpenP: function (day) {
        return round(100 * ((this.priceOpen[day] - this.priceYesterday[day]) / this.priceYesterday[day]), 2)
    },
    priceFinal: null,
    priceFinalC: function (day) {
        return this.priceFinal[day] - this.priceYesterday[day]
    },
    priceFinalP: function (day) {
        return round(100 * ((this.priceFinal[day] - this.priceYesterday[day]) / this.priceYesterday[day]), 2)
    },
    priceClose: null,
    priceCloseC: function (day) {
        return this.priceClose[day] - this.priceYesterday[day]
    },
    priceCloseP: function (day) {
        return round(100 * ((this.priceClose[day] - this.priceYesterday[day]) / this.priceYesterday[day]), 2)
    },
    tradeCount: null,
    tradeVolume: null,
    tradeValue: null,
    priceLow: null,
    priceLowC: function (day) {
        return this.priceLow[day] - this.priceYesterday[day]
    },
    priceLowP: function (day) {
        return round(100 * ((this.priceLow[day] - this.priceYesterday[day]) / this.priceYesterday[day]), 2)
    },
    priceHigh: null,
    priceHighC: function (day) {
        return this.priceHigh[day] - this.priceYesterday[day]
    },
    priceHighP: function (day) {
        return round(100 * ((this.priceHigh[day] - this.priceYesterday[day]) / this.priceYesterday[day]), 2)
    },
    eps: null,
    pe: function () {
        return this.eps == "" ? "" : round(this.priceFinal / this.eps, 2)
    },
    bidCount1: null, bidVolume1: null, bidPrice1: null, askCount1: null, askVolume1: null, askPrice1: null,
    bidCount2: null, bidVolume2: null, bidPrice2: null, askCount2: null, askVolume2: null, askPrice2: null,
    bidCount3: null, bidVolume3: null, bidPrice3: null, askCount3: null, askVolume3: null, askPrice3: null,
    bidCount4: null, bidVolume4: null, bidPrice4: null, askCount4: null, askVolume4: null, askPrice4: null,
    bidCount5: null, bidVolume5: null, bidPrice5: null, askCount5: null, askVolume5: null, askPrice5: null,
    buyCount: null, 
    buyCountLegal: null, 
    buyVolume: null, 
    buyVolumeLegal: null, 
    sellCount: null, 
    sellCountLegal: null, 
    sellVolume: null, 
    sellVolumeLegal: null,
}

export const mainRow = [];
export const indexRow = [];
export const marketDate = {date: {}, day: {}};

export function addMainRow(id, index, row) {

    if (typeof mainRow[id] === 'undefined')
        mainRow[id] = Object.assign({}, rowModel);

    Object.keys(row)
        .forEach(prop => {
            if (index !== false) {
                if (mainRow[id][prop] == null)
                    mainRow[id][prop] = {};
                mainRow[id][prop][index] = row[prop];
            } else {
                mainRow[id][prop] = row[prop];
            }
        })
}

export function addIndexRow(id, index, row) {

    if (typeof indexRow[id] === 'undefined')
        indexRow[id] = {}

    Object.keys(row)
        .forEach(prop => {
            if (typeof indexRow[id][prop] === 'undefined')
                indexRow[id][prop] = {};
            indexRow[id][prop][index] = row[prop];
        })
}

export function addMarketDateRow(day, date) {
    marketDate.date[day] = date;
    marketDate.day[date] = day;
}

export function addRow(id, index, row, dataObj) {

    if (typeof dataObj[id] === 'undefined')
        dataObj[id] = {}

    Object.keys(row)
        .forEach(prop => {
            if (index !== false) {
                if (dataObj[id][prop] == null || typeof dataObj[id][prop] === 'undefined')
                    dataObj[id][prop] = {};
                dataObj[id][prop][index] = row[prop];
            } else {
                dataObj[id][prop] = row[prop];
            }
        })
}

export function TSETMCMainDataParser(APIResponse, refid, heven) {
    const allData = APIResponse.split("@");
    const day = 0;

    //main data
    allData[2].split(";").forEach(rawData => {
        const col = rawData.split(",");
        const rowId = col[0];

        if (col.length === 10 && mainRow[rowId] !== undefined && mainRow[rowId].symbol !== null) {
            addMainRow(rowId, day, {
                // heven: col[1],
                priceOpen: parseFloat(col[2]),
                priceFinal: parseFloat(col[3]),
                priceClose: parseFloat(col[4]),
                tradeCount: parseFloat(col[5]),
                tradeVolume: parseFloat(col[6]),
                tradeValue: parseFloat(col[7]),
                priceLow: parseFloat(col[8]),
                priceHigh: parseFloat(col[9]),
            });

            if (heven < parseInt(col[1]))
                heven = parseInt(col[1])

        } else if (col.length === 23) {

            addMainRow(rowId, false, {
                id: rowId,
                id2: col[1],
                symbol: col[2],
                name: col[3],
                baseVolume: parseFloat(col[15]),
                visitCount: parseFloat(col[16]),
                flow: parseFloat(col[17]),
                industryCode: parseInt(col[18]),
                stockCount: parseFloat(col[21]),
                yval: parseFloat(col[22]),
                heven: col[4],
                eps: parseFloat(col[14]),
            });

            addMainRow(rowId, day, {
                priceYesterday: parseFloat(col[13]),
                priceMax: parseFloat(col[19]),
                priceMin: parseFloat(col[20]),
                priceOpen: parseFloat(col[5]),
                priceFinal: parseFloat(col[6]),
                priceClose: parseFloat(col[7]),
                tradeCount: parseFloat(col[8]),
                tradeVolume: parseFloat(col[9]),
                tradeValue: parseFloat(col[10]),
                priceLow: parseFloat(col[11]),
                priceHigh: parseFloat(col[12]),
            });

            if (heven < parseInt(col[4]))
                heven = parseInt(col[4])

        }
    })

    //ask Bid data
    const askBidData = allData[3].split(";")
    if (askBidData[0] !== "") {
        askBidData.forEach(rawData => {
            const col = rawData.split(",");
            const rowId = col[0];

            if (mainRow[rowId] !== undefined && mainRow[rowId].symbol !== null) {
                const line = col[1];
                const newRow = {};

                newRow['askCount' + line] = parseFloat(col[2]);
                newRow['bidCount' + line] = parseFloat(col[3]);
                newRow['bidPrice' + line] = parseFloat(col[4]);
                newRow['askPrice' + line] = parseFloat(col[5]);
                newRow['bidVolume' + line] = parseFloat(col[6]);
                newRow['askVolume' + line] = parseFloat(col[7]);

                addMainRow(rowId, false, newRow)
            }
        });
    }

    if (allData[4] !== "0" && parseInt(allData[4]) > refid) {
        refid = parseInt(allData[4])
    }

    return {refid, heven}
}

export function TSETMCMainHistoryDataParser(APIResponse) {

    let rows = APIResponse.split(";");
    let col, rowId, offset, day;

    rows.forEach(row => {
        col = row.split(",");
        if (col.length === 11) {
            rowId = col[0];
            offset = 1;
        } else
            offset = 0;
        day = parseInt(col[offset]) + 1;

        addMainRow(rowId, day, {
            priceFinal: parseFloat(col[offset + 1]),
            priceClose: parseFloat(col[offset + 2]),
            tradeCount: parseFloat(col[offset + 3]),
            tradeVolume: parseFloat(col[offset + 4]),
            tradeValue: parseFloat(col[offset + 5]),
            priceLow: parseFloat(col[offset + 6]),
            priceHigh: parseFloat(col[offset + 7]),
            priceYesterday: parseFloat(col[offset + 8]),
            priceOpen: parseFloat(col[offset + 9])
        })
    })
}

export function TSETMCRealLegalDataParser(APIResponse) {

    const rows = APIResponse.split(";");
    const day = 0;
    let col, rowId;

    rows.forEach(row => {
        col = row.split(",");
        rowId = col[0];
        addMainRow(rowId, day, {
            buyCount: parseInt(col[1], 10),
            buyCountLegal: parseInt(col[2], 10),
            buyVolume: parseInt(col[3], 10),
            buyVolumeLegal: parseInt(col[4], 10),
            sellCount: parseInt(col[5], 10),
            sellCountLegal: parseInt(col[6], 10),
            sellVolume: parseInt(col[7], 10),
            sellVolumeLegal: parseInt(col[8], 10)
        })

    })

}

export function TablokhaniMainHistoryDataParser(APIResponse) {

    let rows = APIResponse.split("@");
    let rowsIn, col, rowId, offset, day;
    rows.forEach((row) => {
        rowsIn = row.slice(0, -1).split(";");
        rowsIn.forEach((rowIn) => {
            if (rowIn) {
                col = rowIn.split(",");
                if (col.length === 13) {
                    rowId = col[0];
                    offset = 1;
                } else
                    offset = 0;

                day = parseInt(col[offset]) + 1;
                addMainRow(rowId, day, {
                    priceOpen: parseFloat(col[offset + 1]),
                    priceFinal: parseFloat(col[offset + 3]),
                    priceClose: parseFloat(col[offset + 4]),
                    tradeCount: parseFloat(col[offset + 5]),
                    tradeVolume: parseFloat(col[offset + 6]),
                    tradeValue: parseFloat(col[offset + 7]),
                    // priceChange: {index, value: parseFloat(col[offset + 8])},
                    priceLow: parseFloat(col[offset + 9]),
                    priceHigh: parseFloat(col[offset + 10]),
                    priceYesterday: parseFloat(col[offset + 11]),
                });

                const date = col[offset + 2].toString()
                addMainRow(rowId, date, {
                    dayIndexByDate: parseInt(day),

                })
            }
        })
    })

}

export function TablokhaniRealLegalHistoryDataParser(APIResponse) {

    let rows = APIResponse.split("@");
    let rowsIn, col, rowId, offset, day;

    rows.forEach((row) => {
        rowsIn = row.slice(0, -1).split(";");
        rowsIn.forEach((rowIn) => {
            col = rowIn.split(",");
            if (col.length === 10) {
                rowId = col[0];
                offset = 1;
            } else
                offset = 0;
            day = parseInt(col[offset]) + 1; //day

            addMainRow(rowId, day, {
                buyCount: parseFloat(col[offset + 1]),
                buyCountLegal: parseFloat(col[offset + 2]),
                buyVolume: parseFloat(col[offset + 3]),
                buyVolumeLegal: parseFloat(col[offset + 4]),
                sellCount: parseFloat(col[offset + 5]),
                sellCountLegal: parseFloat(col[offset + 6]),
                sellVolume: parseFloat(col[offset + 7]),
                sellVolumeLegal: parseFloat(col[offset + 8])
            })

        })
    })

}

export function TablokhaniIndexDataParser(APIResponse) {

    const rawData = APIResponse.split("@");

    tablokhaniIndex(rawData[0], 'TEDPIX')
    tablokhaniIndex(rawData[1], 'TEDPIX_EW')

    function tablokhaniIndex(data, id) {
        data.slice(0, -1)
            .split(";")
            .map(row => row.split(","))
            .forEach(cols => {
                const day = parseInt(cols[0]) + 1;
                addIndexRow(id, day,
                    {
                        idxOpen: parseFloat(cols[1]),
                        idxClose: parseFloat(cols[3]),
                        idxHigh: parseFloat(cols[5]),
                        idxLow: parseFloat(cols[6]),
                        idxTradeValue: parseFloat(cols[4]),
                    })

                const date = new Date(cols[2] * 1000)
                const strDate = dateFormatter(date);
                // addIndexRow(id, strDate, {dayIndexByDate: day})
                addMarketDateRow(day, strDate)
            })
    }

    function dateFormatter(date) {
        return date.getFullYear().toString() +
            (date.getMonth() < 9 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1).toString() +
            (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate()).toString();
    }
}


export const industryName = {
    1: "زراعت و خدمات وابسته",
    2: "جنگلداري و ماهيگيري",
    10: "استخراج زغال سنگ",
    11: "استخراج نفت گاز و خدمات جنبي جز اکتشاف",
    13: "استخراج کانه هاي فلزي",
    14: "استخراج ساير معادن",
    17: "منسوجات",
    19: "دباغي، پرداخت چرم و ساخت انواع پاپوش",
    20: "محصولات چوبي",
    21: "محصولات كاغذي",
    22: "انتشار، چاپ و تکثير",
    23: "فراورده هاي نفتي، كك و سوخت هسته اي",
    25: "لاستيك و پلاستيك",
    26: "توليد محصولات كامپيوتري الكترونيكي ونوري",
    27: "فلزات اساسي",
    28: "ساخت محصولات فلزي",
    29: "ماشين آلات و تجهيزات",
    31: "ماشين آلات و دستگاه‌هاي برقي",
    32: "ساخت دستگاه‌ها و وسايل ارتباطي",
    33: "ابزارپزشکي، اپتيکي و اندازه‌گيري",
    34: "خودرو و ساخت قطعات",
    35: "ساير تجهيزات حمل و نقل",
    36: "مبلمان و مصنوعات ديگر",
    38: "قند و شكر",
    39: "شرکتهاي چند رشته اي صنعتي",
    40: "عرضه برق، گاز، بخاروآب گرم",
    41: "جمع آوري، تصفيه و توزيع آب",
    42: "محصولات غذايي و آشاميدني به جز قند و شكر",
    43: "مواد و محصولات دارويي",
    44: "محصولات شيميايي",
    45: "پيمانكاري صنعتي",
    46: "تجارت عمده فروشي به جز وسايل نقليه موتور",
    47: "خرده فروشي،باستثناي وسايل نقليه موتوري",
    49: "كاشي و سراميك",
    50: "تجارت عمده وخرده فروشي وسائط نقليه موتور",
    51: "حمل و نقل هوايي",
    52: "انبارداري و حمايت از فعاليتهاي حمل و نقل",
    53: "سيمان، آهك و گچ",
    54: "ساير محصولات كاني غيرفلزي",
    55: "هتل و رستوران",
    56: "سرمايه گذاريها",
    57: "بانكها و موسسات اعتباري",
    58: "ساير واسطه گريهاي مالي",
    59: "اوراق حق تقدم استفاده از تسهيلات مسكن",
    60: "حمل ونقل، انبارداري و ارتباطات",
    61: "حمل و نقل آبي",
    63: "فعاليت هاي پشتيباني و كمكي حمل و نقل",
    64: "مخابرات",
    65: "واسطه‌گري‌هاي مالي و پولي",
    66: "بيمه وصندوق بازنشستگي به جزتامين اجتماعي",
    67: "فعاليتهاي كمكي به نهادهاي مالي واسط",
    68: "صندوق سرمايه گذاري قابل معامله",
    69: "اوراق تامين مالي",
    70: "انبوه سازي، املاك و مستغلات",
    71: "فعاليت مهندسي، تجزيه، تحليل و آزمايش فني",
    72: "رايانه و فعاليت‌هاي وابسته به آن",
    73: "اطلاعات و ارتباطات",
    74: "خدمات فني و مهندسي",
    76: "اوراق بهادار مبتني بر دارايي فكري",
    77: "فعالبت هاي اجاره و ليزينگ",
    82: "فعاليت پشتيباني اجرائي اداري وحمايت كسب",
    90: "فعاليت هاي هنري، سرگرمي و خلاقانه",
    93: "فعاليتهاي فرهنگي و ورزشي",
    98: "گروه اوراق غيرفعال",
};

export const industryShortName = {
    1: "زراعت",
    2: "جنگلداري",
    10: "زغال‌سنگ",
    11: "استخراج نفت گاز",
    13: "کانه‌هاي فلزي",
    14: "ساير معادن",
    17: "منسوجات",
    19: "دباغي",
    20: "محصولات چوبي",
    21: "محصولات كاغذي",
    22: "چاپ",
    23: "فراورده‌هاي نفتي",
    25: "لاستيك و پلاستيك",
    26: "محصولات كامپيوتري",
    27: "فلزات اساسي",
    28: "محصولات فلزي",
    29: "ماشين‌آلات",
    31: "دستگاه‌هاي برقي",
    32: "وسايل ارتباطي",
    33: "ابزارپزشکي",
    34: "خودرو و قطعات",
    35: "تجهيزات حمل‌ونقل",
    36: "مبلمان",
    38: "قند و شكر",
    39: "چندرشته‌اي صنعتي",
    40: "برق-گاز-بخار-آب",
    41: "تصفيه و توزيع آب",
    42: "محصولات غذايي",
    43: "دارويي",
    44: "شيميايي",
    45: "پيمانكاري صنعتي",
    46: "تجارت عمده فروشي",
    47: "خرده فروشي",
    49: "كاشي و سراميك",
    53: "سيمان",
    54: "كاني غيرفلزي",
    55: "هتل و رستوران",
    56: "سرمايه گذاري‌ها",
    57: "بانك‌ها",
    58: "ساير واسطه‌گري‌هاي",
    60: "حمل‌ونقل و انبارداري",
    61: "حمل و نقل آبي",
    64: "مخابرات",
    65: "واسطه‌گري‌ها",
    66: "بيمه",
    67: "كمك به نهادهاي مالي",
    68: "صندوق",
    70: "انبوه سازي",
    71: "فعاليت مهندسي",
    72: "رايانه",
    73: "اطلاعات‌وارتباطات",
    74: "خدمات فني‌ومهندسي",
    90: "فعاليت‌هاي هنري",
    93: "فرهنگي و ورزشي",
};

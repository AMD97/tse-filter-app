
const KEY = 'filter_conf';

export function add(value) {
    let storage = localStorage.getItem(KEY);
    storage = storage ? JSON.parse(storage) : [];
    if (value.id && storage.filter(item => item.id === value.id).length) { // update operation
        storage[storage.findIndex(el => el.id === value.id)] = value;
    } else { // create operation (auto generate id)
        let id = Math.max(...storage.map(o => o.id), 0);
        value.id = ++id;
        storage.push(value);
    }
    localStorage.setItem(KEY, JSON.stringify(storage));
    return value.id;
}

export function get(key) {
    let storage = JSON.parse(localStorage.getItem(KEY));
    if (!storage || !storage.length) return null;
    let result = storage.filter(item => item.id === key);
    return result.length ? result[0] : null;
}

export function getList() {
    let storage = JSON.parse(localStorage.getItem(KEY));
    if (!storage || !storage.length) return null;
    return storage.filter(item => [item.id, item.title]);
}

export function remove(key) {
    key = parseInt(key)
    let storage = JSON.parse(localStorage.getItem(KEY));
    if (!storage || !storage.length) return null;
    if (storage.filter(item => item.id === key).length) {
        storage = storage.filter(item => item.id !== key);
        localStorage.setItem(KEY, JSON.stringify(storage));
        return true;
    } else
        return false;
}

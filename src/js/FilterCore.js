import {indexRow, industryShortName, mainRow, marketDate} from './Data.js'
import {aliasTransformer} from './Alias.js'

export class FilterCore {

    constructor(filterConfig) {
        this.filterConfig = filterConfig;
        this.filterFunction = null;
        this.filterArgs = null;
        this.filterFirstRun = true;
        this.filteredData = [];
        this.date = false; // false value for last day
    }

    prepareFilter() {

        if (this.filterConfig.code === "") return;

        const code = aliasTransformer(this.filterConfig.code)

        try {
            this.filterFunction = new Function(`
return function ({
              mainData,
              offsetDay,
              indexRow,
              indexOffsetDay,
              globalMem,
              industryShortName,
          }) {

           ${code}
           
            return {_init, _compute, _filter, _fields, _sortProperty};
        }
        `)();

            this.filterArgs = {
                mainData: null,
                offsetDay: null,
                indexRow,
                indexOffsetDay: null,
                globalMem: {},
                industryShortName: industryShortName,
            };

            this.filterFirstRun = true;

        } catch (e) {
            console.error(e);
        }
    }

    filterData() {

        //run _init function
        if (this.filterFirstRun === true) {
            this._filter((args) => this.filterFunction(args)._init());
            this.filterFirstRun = false;
        }

        //run _compute function
        this._filter((args) => this.filterFunction(args)._compute());

        //run _filter and _fields function and return filtered data(field and sortProperty)
        this.filteredData = this._filter((args) => {

                const fn = this.filterFunction(args);

                if (fn._filter() === true) {
                    return {
                        passed: true,
                        output: {
                            field: fn._fields(),
                            sort: fn._sortProperty
                        }
                    }
                } else {
                    return {passed: false}
                }

            },
            (outputArray, result) => {
                if (result.passed === true) outputArray.push(result.output);
                return outputArray;
            },
            []
        );
    }

    _filter(fnCall, fnOutputHandler, outputInit) {

        let output = outputInit;

        for (let id in mainRow) {

            if (!mainRow.hasOwnProperty(id) || !mainRow[id].symbol) continue;

            const mainData = mainRow[id];

            if (!this._assetFilter(mainData)) continue;

            if (!this._industryFilter(mainData)) continue;

            const {offsetDay, indexOffsetDay} = this._getOffsetDay(mainData);
            if (offsetDay == undefined || indexOffsetDay == undefined) continue;

            this.filterArgs.mainData = mainData;
            this.filterArgs.offsetDay = offsetDay;
            this.filterArgs.indexOffsetDay = indexOffsetDay;

            try {
                const result = fnCall(this.filterArgs);

                if (typeof fnOutputHandler === 'function') {
                    output = fnOutputHandler(output, result)
                }

            } catch (e) {
                console.log(e.toString())
            }

        }

        return output;
    }

    _assetFilter(mainData) {
        const yval = mainData.yval;
        return ((!this.filterConfig.asset.stockExchange && (yval === 300)) ||
            (!this.filterConfig.asset.stockOTC && (yval === 303 || yval === 313)) ||
            (!this.filterConfig.asset.basisOTC && (yval === 309)) ||
            (!this.filterConfig.asset.priority && (yval === 400 || yval === 403 || yval === 404)) |
            (!this.filterConfig.asset.debtSecurity && (yval === 306 || yval === 301 || yval === 706 || yval === 208 || yval === 206)) |
            (!this.filterConfig.asset.future && (yval === 263)) ||
            (!this.filterConfig.asset.fund && (yval === 305 || yval === 380)) ||
            (!this.filterConfig.asset.option && (yval === 600 || yval === 602 || yval === 605 || yval === 311 || yval === 312 || yval === 320)) ||
            (!this.filterConfig.asset.commodity && (yval === 308 || yval === 701)) ||
            (!this.filterConfig.asset.housingBonds && (yval === 307))) ? false : true;
    }

    _industryFilter(mainData) {
        return (!this.filterConfig.industry.all &&
            !this.filterConfig.industry.selected.includes(mainData.industryCode.toString())) ? false : true;
    }

    _getOffsetDay(mainData) { //return undefined if OffsetDay not available
        const offsetDay = (this.date === false) ? 0 :
            mainData?.dayIndexByDate?.[this.date];

        const indexOffsetDay = (this.date === false) ? 0 :
            marketDate?.day?.[this.date];

        return {offsetDay, indexOffsetDay};
    }

}

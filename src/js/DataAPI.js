import {
    TablokhaniIndexDataParser,
    TablokhaniMainHistoryDataParser,
    TablokhaniRealLegalHistoryDataParser,
    TSETMCMainDataParser,
    TSETMCMainHistoryDataParser,
    TSETMCRealLegalDataParser,
} from "./Data.js";

const DEFAULT_INTERVAL = 4 * 1000;
const DEFAULT_RETRY = 10;

let refid = 0; // tsetmc api parameter
let heven = 0; // tsetmc api parameter

export function startDataLoader() {

    myFetch(
        TSETSEMainDataURLGenerator,
        (textData) => ({refid, heven} = TSETMCMainDataParser(textData, refid, heven)),
        DEFAULT_INTERVAL
    );

    myFetch(
        'http://www.tsetmc.com/tsev2/data/ClosingPriceAll.aspx',
        TSETMCMainHistoryDataParser
    );

    myFetch(
        'http://www.tsetmc.com/tsev2/data/ClientTypeAll.aspx',
        TSETMCRealLegalDataParser,
        DEFAULT_INTERVAL
    );

    myFetch(
        'https://api4.tablokhani.com/Process/oldClientFilter',
        TablokhaniRealLegalHistoryDataParser
    );

    myFetch(
        'https://api2.tablokhani.com/Process/Shakhes',
        TablokhaniIndexDataParser
    );

    fetchParallel(
        [
            'https://api4.tablokhani.com/Process/oldTradeRowsPart1',
            'https://api4.tablokhani.com/Process/oldTradeRowsPart2'
        ],
        (dataArray) => {
            TablokhaniMainHistoryDataParser(dataArray[0] + dataArray[1])
        }
    )
}

async function myFetch(url, parserFn, interval, retry = DEFAULT_RETRY) {

    let errorOccurred = false;

    try {

        let urlStr = (typeof url === 'function') ? url() : url;
        const response = await fetch(urlStr);

        if (!response.ok) {
            const message = `An error has occurred: ${response.status}`;
            throw new Error(message);
        }

        const text = await response.text();

        parserFn(text);

        retry = DEFAULT_RETRY;

    } catch (e) {

        console.error(e);
        errorOccurred = true;
        retry--;

    } finally {

        if ((errorOccurred && retry > 0) || interval) {

            setTimeout(myFetch.bind(null, url, parserFn, interval, retry),
                isFinite(interval) ? interval : DEFAULT_INTERVAL)

        }

    }
}

async function fetchParallel(urlArray, fnParser, retry = DEFAULT_RETRY) {

    try {

        const requests = urlArray.map((url) => fetch(url));
        const responses = await Promise.all(requests);
        const errors = responses.filter((response) => !response.ok);

        if (errors.length > 0) {
            throw errors.map((response) => Error(response.statusText));
        }

        const textArray = responses.map((response) => response.text());

        const dataArray = await Promise.all(textArray);

        fnParser(dataArray);

    } catch (errors) {

        errors.forEach((error) => console.error(error));

        setTimeout(fetchParallel.bind(null, urlArray, fnParser, retry - 1), DEFAULT_INTERVAL);

    }
}

function TSETSEMainDataURLGenerator(interval) {

    let url = (heven === 0) ?
        "http://www.tsetmc.com/tsev2/data/MarketWatchInit.aspx?" :
        "http://www.tsetmc.com/tsev2/data/MarketWatchPlus.aspx?"

    url += new URLSearchParams({
        h: 5 * Math.floor(heven / 5),
        r: 25 * Math.floor(refid / 25)
    }).toString();

    return url;
}
import {FilterCore} from "./FilterCore.js";

export class TableFilter extends FilterCore {

    constructor(filterConfig, tableContentId, sortConfig) {

       super(filterConfig);
        this.timer = null;
        this.tableContentId = tableContentId;
        this.sortConfig = sortConfig ?
            sortConfig :
            {
                type: null,       // 'field' or 'property'
                parameter: null,  // field number or property value
                desc: false
            };
    }

    initTable() {

        let table = document.getElementById('tableTemplate').content.cloneNode(true);
        document.getElementById(this.tableContentId).appendChild(table);
    }

    initSortMenu() {

        let sortMenu = document.getElementById('sortMenuTemplate').content.firstElementChild.cloneNode(true);

        sortMenu.querySelector('.dropdown-menu')
            .addEventListener('click', ev => {
                if (ev.target.nodeName !== 'BUTTON') ev.stopPropagation()
            });

        sortMenu.querySelector('.dropdown-menu button')
            .addEventListener('click', this._sortBtnListener.bind(this));

        let parent = document.querySelector(`#${this.tableContentId} table caption .btn-group`);
        parent.insertBefore(sortMenu, parent.childNodes[0]);
    }

    renderSortMenuItem() {

        document.querySelector(`#${this.tableContentId} .dropdown-menu .sortByFieldList`)
            .innerHTML = this.filterConfig.template
            .filter(t => t.active)
            .reduce((total, template) => {
                return `${total}
                             <label class="dropdown-item-text form-check-label">
                                <input class="form-check-input me-1" type="radio" name="sortRadio" value="${template.field}" data-type="field">
                                ${template.title}
                            </label> `
            }, '');


        if (this.filterConfig.sortProperty) {
            debugger
            let properties = this.filterConfig.sortProperty
                .split('\n')
                .map(p => {
                    let property = p.split(':')
                    return {property: property[0].trim(), title: property[1].trim()}
                });

            document.querySelector(`#${this.tableContentId} .dropdown-menu .sortByPropertyList`)
                .innerHTML = properties
                .reduce((total, prop) => {
                    return `${total}
                             <label class="dropdown-item-text form-check-label">
                                <input class="form-check-input me-1" type="radio" name="sortRadio" value="${prop.property}" data-type="property">
                                ${prop.title}
                            </label> `
                }, '');
        }
    }

    _sortBtnListener(ev) {

        const parameterRadio = document.querySelector(`#${this.tableContentId} .dropdown-menu .sortParameter input:checked`);
        const descRadio = document.querySelector(`#${this.tableContentId} .dropdown-menu .descRadio input:checked`);

        if (parameterRadio && descRadio) {
            this.sortConfig.desc = (descRadio.value === 'desc');

            const iconElem = document.querySelector(`#${this.tableContentId} .sortMenu > button > i`);
            iconElem.classList.toggle('fa-sort-alpha-down', !this.sortConfig.desc)
            iconElem.classList.toggle('fa-sort-alpha-up-alt', this.sortConfig.desc)

            this.sortConfig.type = parameterRadio.dataset.type;
            this.sortConfig.parameter = parameterRadio.value;
            document.querySelector(`#${this.tableContentId} .sortMenu > button > span.title`)
                .innerText = parameterRadio.parentElement.innerText;
            this._resortTableData();
        }
    }

    _sortData() {

        const type = this.sortConfig.type;
        const parameter = this.sortConfig.parameter;
        const desc = this.sortConfig.desc;

        const compareByAlphabet = (one, two, desc) => {
            return desc ?
                two.localeCompare(one, undefined, {numeric: true}) :
                one.localeCompare(two, undefined, {numeric: true})
        }
        const compareByNumber = (one, two, desc) => {
            return desc ?
                two - one :
                one - two
        }

        const sorting = (a, b) => {

            let one, two;
            if (type === 'property' && a.sort[parameter] && b.sort[parameter]) {

                [one, two] = [a.sort[parameter].toString(), b.sort[parameter].toString()];

            } else {

                if (type === 'field' && a.field[parameter] && b.field[parameter]) {
                    [one, two] = [a.field[parameter].toString(), b.field[parameter].toString()];
                } else {
                    [one, two] = [null, null];
                }
            }

            if (isNaN(one) || isNaN(two)) {
                return !(one && two) ? 0 : compareByAlphabet(one, two, desc)
            } else {
                return !(one && two) ? 0 : compareByNumber(one, two, desc)
            }
        }

        this.filteredData.sort(sorting);

        console.log(`${this.tableContentId} sorted ${new Date().toLocaleTimeString()}`);
    }

    _renderTableData() {

        document.querySelector(`#${this.tableContentId} thead`)
            .innerHTML = this.filterConfig.template
            .filter(t => t.active)
            .reduce((totalTd, curTd) => {
                return `${totalTd}<td>${curTd.title}</td> `
            }, '');

        const activeField = this.filterConfig.template.filter(t => t.active).map(f => parseInt(f.field));

        document.querySelector(`#${this.tableContentId} tbody`)
            .innerHTML = JSON.parse(JSON.stringify(this.filteredData))
            .map((row) => {
                const renderField = [];
                let idx = 0;
                activeField.forEach(i => renderField[idx++] = row.field[i])
                return renderField;
            })
            .reduce((totalRow, row) => {
                return `${totalRow}<tr> ${row
                    .reduce((totalTd, field) => {
                        return `${totalTd}<td>${field}</td> `
                    }, '')
                }</tr> `
            }, '');

        document.querySelector(`#${this.tableContentId} .filter-date`)
            .textContent = this.date ? this.date : 'آخرین روز معاملاتی';
        document.querySelector(`#${this.tableContentId} .filter-last-update`)
            .textContent = new Date().toLocaleTimeString();
        document.querySelector(`#${this.tableContentId} .filter-row-count`)
            .textContent = this.filteredData.length.toString();

        console.log(`${this.tableContentId} updated ${new Date().toLocaleTimeString()}`);
    }

    _filtering() {
        this.filterData();
        this._sortData();
        this._renderTableData();
    }

    _resortTableData() {
        this._sortData();
        this._renderTableData();
    }

    startFiltering() {
        if (this.timer !== undefined)
            window.clearTimeout(this.timer);

        this._filtering();

        this.timer = setTimeout(this.startFiltering.bind(this), this.filterConfig.intervalTime * 1000);
    }

    stopFiltering() {
        window.clearTimeout(this.timer);
    }

}

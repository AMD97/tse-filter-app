

export function aliasTransformer(srcCode) {

    for (let {srcAlias, object, property, index, dayOffset} of alias) {

        const regex0 = new RegExp(srcAlias, 'gi')

        if (!regex0.test(srcCode)) continue;

        if (index !== false) { //insert default [0] index if not available
            const regex1 = new RegExp(`(?<=(${srcAlias}))(?!(\\[|\\(|\\w)+?)`, 'gi');
            srcCode = srcCode.replace(regex1, index.replace('*', '0'));  // '_priceValue' to '_priceValue[0]'
        }

        if (dayOffset !== false) {
            const regex2 = new RegExp(`(?<=(${srcAlias}\\${index[0]}))`, 'gi');
            srcCode = srcCode.replace(regex2, `${dayOffset} + `);  // '_priceValue[0]' to '_priceValue[offsetDay + 0]'
        }

        const regex3 = new RegExp(`${srcAlias}(?!(\\w)+?)`, 'gi');
        srcCode = srcCode.replace(regex3, object + '.' + property); // '_priceValue' to 'mainData.priceValue'
    }

    return srcCode;
}

const alias = [

    {srcAlias: '_id', object: 'mainData', property: 'id', index: false, dayOffset: false},
    {srcAlias: '_id2', object: 'mainData', property: 'id2', index: false, dayOffset: false},
    {srcAlias: '_symbol', object: 'mainData', property: 'symbol', index: false, dayOffset: false},
    {srcAlias: '_name', object: 'mainData', property: 'name', index: false, dayOffset: false},
    {srcAlias: '_industryCode', object: 'mainData', property: 'industryCode', index: false, dayOffset: false},
    {srcAlias: '_visitCount', object: 'mainData', property: 'visitCount', index: false, dayOffset: false},
    {srcAlias: '_flow', object: 'mainData', property: 'flow', index: false, dayOffset: false},
    {srcAlias: '_yval', object: 'mainData', property: 'yval', index: false, dayOffset: false},
    {srcAlias: '_baseVolume', object: 'mainData', property: 'baseVolume', index: false, dayOffset: false},
    {srcAlias: '_stockCount', object: 'mainData', property: 'stockCount', index: false, dayOffset: false},
    {srcAlias: '_eps', object: 'mainData', property: 'eps', index: false, dayOffset: false},
    {srcAlias: '_pe', object: 'mainData', property: 'pe()', index: false, dayOffset: false},

    {srcAlias: '_bidCount1', object: 'mainData', property: 'bidCount1', index: false, dayOffset: false},
    {srcAlias: '_bidCount2', object: 'mainData', property: 'bidCount2', index: false, dayOffset: false},
    {srcAlias: '_bidCount3', object: 'mainData', property: 'bidCount3', index: false, dayOffset: false},
    {srcAlias: '_bidCount4', object: 'mainData', property: 'bidCount4', index: false, dayOffset: false},
    {srcAlias: '_bidCount5', object: 'mainData', property: 'bidCount5', index: false, dayOffset: false},
    {srcAlias: '_bidVolume1', object: 'mainData', property: 'bidVolume1', index: false, dayOffset: false},
    {srcAlias: '_bidVolume2', object: 'mainData', property: 'bidVolume2', index: false, dayOffset: false},
    {srcAlias: '_bidVolume3', object: 'mainData', property: 'bidVolume3', index: false, dayOffset: false},
    {srcAlias: '_bidVolume4', object: 'mainData', property: 'bidVolume4', index: false, dayOffset: false},
    {srcAlias: '_bidVolume5', object: 'mainData', property: 'bidVolume5', index: false, dayOffset: false},
    {srcAlias: '_bidPrice1', object: 'mainData', property: 'bidPrice1', index: false, dayOffset: false},
    {srcAlias: '_bidPrice2', object: 'mainData', property: 'bidPrice2', index: false, dayOffset: false},
    {srcAlias: '_bidPrice3', object: 'mainData', property: 'bidPrice3', index: false, dayOffset: false},
    {srcAlias: '_bidPrice4', object: 'mainData', property: 'bidPrice4', index: false, dayOffset: false},
    {srcAlias: '_bidPrice5', object: 'mainData', property: 'bidPrice5', index: false, dayOffset: false},
    {srcAlias: '_askCount1', object: 'mainData', property: 'askCount1', index: false, dayOffset: false},
    {srcAlias: '_askCount2', object: 'mainData', property: 'askCount2', index: false, dayOffset: false},
    {srcAlias: '_askCount3', object: 'mainData', property: 'askCount3', index: false, dayOffset: false},
    {srcAlias: '_askCount4', object: 'mainData', property: 'askCount4', index: false, dayOffset: false},
    {srcAlias: '_askCount5', object: 'mainData', property: 'askCount5', index: false, dayOffset: false},
    {srcAlias: '_askVolume1', object: 'mainData', property: 'askVolume1', index: false, dayOffset: false},
    {srcAlias: '_askVolume2', object: 'mainData', property: 'askVolume2', index: false, dayOffset: false},
    {srcAlias: '_askVolume3', object: 'mainData', property: 'askVolume3', index: false, dayOffset: false},
    {srcAlias: '_askVolume4', object: 'mainData', property: 'askVolume4', index: false, dayOffset: false},
    {srcAlias: '_askVolume5', object: 'mainData', property: 'askVolume5', index: false, dayOffset: false},
    {srcAlias: '_askPrice1', object: 'mainData', property: 'askPrice1', index: false, dayOffset: false},
    {srcAlias: '_askPrice2', object: 'mainData', property: 'askPrice2', index: false, dayOffset: false},
    {srcAlias: '_askPrice3', object: 'mainData', property: 'askPrice3', index: false, dayOffset: false},
    {srcAlias: '_askPrice4', object: 'mainData', property: 'askPrice4', index: false, dayOffset: false},
    {srcAlias: '_askPrice5', object: 'mainData', property: 'askPrice5', index: false, dayOffset: false},
    {srcAlias: '_priceYesterday', object: 'mainData', property: 'priceYesterday', index: '[*]', dayOffset: 'offsetDay'},
    {srcAlias: '_priceOpen', object: 'mainData', property: 'priceOpen', index: '[*]', dayOffset: 'offsetDay'},
    {srcAlias: '_priceOpenC', object: 'mainData', property: 'priceOpenC', index: '(*)', dayOffset: 'offsetDay'},
    {srcAlias: '_priceOpenP', object: 'mainData', property: 'priceOpenP', index: '(*)', dayOffset: 'offsetDay'},
    {srcAlias: '_priceClose', object: 'mainData', property: 'priceClose', index: '[*]', dayOffset: 'offsetDay'},
    {srcAlias: '_priceCloseC', object: 'mainData', property: 'priceCloseC', index: '(*)', dayOffset: 'offsetDay'},
    {srcAlias: '_priceCloseP', object: 'mainData', property: 'priceCloseP', index: '(*)', dayOffset: 'offsetDay'},
    {srcAlias: '_priceFinal', object: 'mainData', property: 'priceFinal', index: '[*]', dayOffset: 'offsetDay'},
    {srcAlias: '_priceFinalC', object: 'mainData', property: 'priceFinalC', index: '(*)', dayOffset: 'offsetDay'},
    {srcAlias: '_priceFinalP', object: 'mainData', property: 'priceFinalP', index: '(*)', dayOffset: 'offsetDay'},
    {srcAlias: '_priceLow', object: 'mainData', property: 'priceLow', index: '[*]', dayOffset: 'offsetDay'},
    {srcAlias: '_priceLowC', object: 'mainData', property: 'priceLowC', index: '(*)', dayOffset: 'offsetDay'},
    {srcAlias: '_priceLowP', object: 'mainData', property: 'priceLowP', index: '(*)', dayOffset: 'offsetDay'},
    {srcAlias: '_priceHigh', object: 'mainData', property: 'priceHigh', index: '[*]', dayOffset: 'offsetDay'},
    {srcAlias: '_priceHighC', object: 'mainData', property: 'priceHighC', index: '(*)', dayOffset: 'offsetDay'},
    {srcAlias: '_priceHighP', object: 'mainData', property: 'priceHighP', index: '(*)', dayOffset: 'offsetDay'},
    {srcAlias: '_priceMin', object: 'mainData', property: 'priceMin', index: '[*]', dayOffset: 'offsetDay'},
    {srcAlias: '_priceMinP', object: 'mainData', property: 'priceMinP', index: '(*)', dayOffset: 'offsetDay'},
    {srcAlias: '_priceMax', object: 'mainData', property: 'priceMax', index: '[*]', dayOffset: 'offsetDay'},
    {srcAlias: '_priceMaxP', object: 'mainData', property: 'priceMaxP', index: '(*)', dayOffset: 'offsetDay'},
    {srcAlias: '_tradeCount', object: 'mainData', property: 'tradeCount', index: '[*]', dayOffset: 'offsetDay'},
    {srcAlias: '_tradeVolume', object: 'mainData', property: 'tradeVolume', index: '[*]', dayOffset: 'offsetDay'},
    {srcAlias: '_tradeValue', object: 'mainData', property: 'tradeValue', index: '[*]', dayOffset: 'offsetDay'},
    {srcAlias: '_buyCount', object: 'mainData', property: 'buyCount', index: '[*]', dayOffset: 'offsetDay'},
    {srcAlias: '_buyCountLegal', object: 'mainData', property: 'buyCountLegal', index: '[*]', dayOffset: 'offsetDay'},
    {srcAlias: '_buyVolume', object: 'mainData', property: 'buyVolume', index: '[*]', dayOffset: 'offsetDay'},
    {srcAlias: '_buyVolumeLegal', object: 'mainData', property: 'buyVolumeLegal', index: '[*]', dayOffset: 'offsetDay'},
    {srcAlias: '_sellCount', object: 'mainData', property: 'sellCount', index: '[*]', dayOffset: 'offsetDay'},
    {srcAlias: '_sellCountLegal', object: 'mainData', property: 'sellCountLegal', index: '[*]', dayOffset: 'offsetDay'},
    {srcAlias: '_sellVolume', object: 'mainData', property: 'sellVolume', index: '[*]', dayOffset: 'offsetDay'},
    {srcAlias: '_sellVolumeLegal', object: 'mainData', property: 'sellVolumeLegal', index: '[*]', dayOffset: 'offsetDay'},

    {srcAlias: '_idxOpen', object: 'indexRow.TEDPIX', property: 'idxOpen', index: '[*]', dayOffset: 'indexOffsetDay'},
    {srcAlias: '_idxClose', object: 'indexRow.TEDPIX', property: 'idxClose', index: '[*]', dayOffset: 'indexOffsetDay'},
    {srcAlias: '_idxHigh', object: 'indexRow.TEDPIX', property: 'idxHigh', index: '[*]', dayOffset: 'indexOffsetDay'},
    {srcAlias: '_idxLow', object: 'indexRow.TEDPIX', property: 'idxLow', index: '[*]', dayOffset: 'indexOffsetDay'},
    {srcAlias: '_idxTradeValue', object: 'indexRow.TEDPIX', property: 'idxTradeValue', index: '[*]', dayOffset: 'indexOffsetDay'},

    {srcAlias: '_idxOpenEW', object: 'indexRow.TEDPIX_EW', property: 'idxOpen', index: '[*]', dayOffset: 'indexOffsetDay'},
    {srcAlias: '_idxCloseEW', object: 'indexRow.TEDPIX_EW', property: 'idxClose', index: '[*]', dayOffset: 'indexOffsetDay'},
    {srcAlias: '_idxHighEW', object: 'indexRow.TEDPIX_EW', property: 'idxHigh', index: '[*]', dayOffset: 'indexOffsetDay'},
    {srcAlias: '_idxLowEW', object: 'indexRow.TEDPIX_EW', property: 'idxLow', index: '[*]', dayOffset: 'indexOffsetDay'},
    {srcAlias: '_idxTradeValueEW', object: 'indexRow.TEDPIX_EW', property: 'idxTradeValue', index: '[*]', dayOffset: 'indexOffsetDay'},

];

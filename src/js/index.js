import {startDataLoader} from './DataAPI.js'
import {industryName, industryShortName} from './Data.js'
import {TableFilter} from './TableFilter.js'
import * as Storage from './LocalStorageManager.js'
import {round, dateToGregorian} from './Utility.js'
import {filterConfigDefault} from './FilterConfig.js'


var filterConfigCurrent;
var editor;
var tfe = [];

(function initial() {
    startDataLoader();
    initColumnTemplateGroupList()
    initIndustryDropdownList()
    initEditor();
    initFilterSetting();
    initDatePicker();
})()

function initFilterSetting() {
    if (!Storage.getList()) { //if storage empty
        Storage.add(filterConfigDefault);
    }
    updateFilterList();
    showFilterConfig(1); //load default filter
}

function initEditor() {
    editor = ace.edit("codeEditor");
    editor.setTheme("ace/theme/dracula");
    editor.session.setMode("ace/mode/javascript");
    editor.setOptions({
        fontSize: "14px",
        animatedScroll: true,
        showPrintMargin: false,
        enableBasicAutocompletion: true,
        enableLiveAutocompletion: true,
        enableSnippets: true,
    });
    editor.commands.addCommand({
        name: "fullscreen",
        bindKey: "F11",
        exec: function (editor) {
            const toggle = document.body.classList.toggle('fullScreen');
            document.getElementById('codeEditor').classList.toggle('fullScreen', toggle)
            editor.setAutoScrollEditorIntoView(!toggle)
            editor.resize()
        }
    })
}

function initColumnTemplateGroupList() {
    let elemStr = `
            <div class="list-group-item list-group-item-action d-inline-flex  align-items-center justify-content-around p-2 draggable">
                <div class="form-check form-switch">
                    <input class="form-check-input" type="checkbox">
                </div>
                <label class="form-check-label ms-2 field" data-field>fieldX</label>
                <input class="form-control w-50 ms-2 title" type="text" placeholder="عنوان">
                <input class="form-control w-25 ms-2 width hidden" type="text" placeholder="عرض" disabled>
            </div>`;

    let groupList = document.querySelector("#columnTemplateGroupList");
    for (let i = 0; i <= 15; i++) groupList.insertAdjacentHTML("beforeend", elemStr)
    Sortable.create(columnTemplateGroupList, {
        animation: 150
    });
}

function initIndustryDropdownList() {
    document.getElementById(`industryDropdownItems`).innerHTML =
        Object.entries(industryShortName)
            .reduce((total, ind) => {
                return `${total}<button class="dropdown-item" data-id="${ind[0]}">${ind[0]} - ${ind[1]}</button>`
            }, '');
}

function initDatePicker() {
    $('#datePicker').persianDatepicker({
        format: 'YYYY/MM/DD',
        observer: true,
        calendar: {
            persian: {
                locale: "en",
                showHint: true,
                leapYearMode: "algorithmic"
            },
            gregorian: {
                locale: "en",
                showHint: true
            },
            toolbox: {
                enabled: true,
                calendarSwitch: {
                    enabled: true,
                    format: "YYYY/MM/DD"
                },
            }
        }
    });

}




// update dropdown filter list
function updateFilterList() {

    // console.log("updateFilterList: ");
    let list = Storage.getList();

    let lastSelected = document.querySelector('#filterListDropdown li div.active');
    let lastSelectedId;
    if (lastSelected)
        lastSelectedId = parseInt(document.querySelector('#filterListDropdown li div.active').dataset.id);

    let divider = document.getElementById("dropdown-divider");
    while (divider.nextElementSibling) divider.nextElementSibling.remove();

    list.forEach(item => {
        if (item.id !== 1) {
            document.querySelector('#filterListDropdown > ul')
                .insertAdjacentHTML('beforeend', `
                    <li>
                        <div class="dropdown-item d-flex align-items-center" data-id="${item.id}">
                            <div class="me-auto">${item.title}</div>
                            <i class="far fa-trash-alt link-danger"></i>
                        </div>
                    </li>
                `)
        }
    })

    lastSelected = document.querySelector(`#filterListDropdown li div[data-id='${lastSelectedId}']`);
    if (lastSelected)
        lastSelected.classList.add('active');

}

function createFilterConfig(title) {
    console.log("createFilterConfig: " + title);
    let newFilterConfig = filterConfigDefault;
    newFilterConfig.id = 0;
    newFilterConfig.title = title;
    let id = Storage.add(newFilterConfig);
    // console.log(`createFilterSetting: title:${newFilterConfig.title}  id:${newFilterConfig.id} saved`);
    updateFilterList();
    showFilterConfig(id);
}

function showFilterConfig(id) {
    id = parseInt(id);
    console.log(`showFilterConfig: id:${id}`);
    let filterConfig = Storage.get(id);
    if (filterConfig) {

        let el = document.querySelector('#filterListDropdown li div.active');
        if (el) el.classList.remove('active');
        document.querySelector(`#filterListDropdown li div[data-id='${id}']`).classList.add('active');
        document.querySelector('#filterListDropdown > button').innerText = filterConfig.title

        document.querySelectorAll('#columnTemplateGroupList > div.list-group-item')
            .forEach((el, i) => {
                el.querySelector('label.field').innerText = 'field[' + filterConfig.template[i].field + ']';
                el.querySelector('label.field').dataset.field = filterConfig.template[i].field;
                el.querySelector('input.form-check-input').checked = filterConfig.template[i].active;
                el.querySelector('input.title').value = filterConfig.template[i].title;
            })

        deselectAllIndustryItem();
        filterConfig.industry.all && selectIndustryItem('all');
        filterConfig.industry.selected.forEach(id => selectIndustryItem(id));
        updateIndustryCaption();

        document.getElementById("stockExchangeCheck").checked = filterConfig.asset.stockExchange;
        document.getElementById("stockOTCCheck").checked = filterConfig.asset.stockOTC;
        document.getElementById("basisOTCCheck").checked = filterConfig.asset.basisOTC;
        document.getElementById("priorityCheck").checked = filterConfig.asset.priority;
        document.getElementById("fundCheck").checked = filterConfig.asset.fund;
        document.getElementById("debtSecurityCheck").checked = filterConfig.asset.debtSecurity;
        document.getElementById("optionCheck").checked = filterConfig.asset.option;
        document.getElementById("commodityCheck").checked = filterConfig.asset.commodity;
        document.getElementById("futureCheck").checked = filterConfig.asset.future;

        setIntervalTime(filterConfig.intervalTime);

        document.getElementById("sortPropertyTextarea").value = filterConfig.sortProperty;
        document.getElementById("cssInjectTextarea").value = filterConfig.css;

        editor.setValue(filterConfig.code);

        filterConfigCurrent = filterConfig;

        // console.log(`showFilterConfig: title:${filterConfigCurrent.title}  id:${filterConfigCurrent.id} loaded`);
    }
}

function updateCurrentFilterConfig() {
    // console.log(`updateCurrentFilterConfig:`);

    document.querySelectorAll('#columnTemplateGroupList > div.list-group-item')
        .forEach((el, i) => {
            filterConfigCurrent.template[i].field = el.querySelector('label.field').dataset.field;
            filterConfigCurrent.template[i].active = el.querySelector('input.form-check-input').checked;
            filterConfigCurrent.template[i].title = el.querySelector('input.title').value;
        })

    filterConfigCurrent.industry.all =
        document.querySelector(`#industryDropdownMenu button[data-id='all']`)
            .classList.contains('active');

    filterConfigCurrent.industry.selected = [];
    document.querySelectorAll(`#industryDropdownItems button`)
        .forEach(el => {
            if (el.classList.contains('active'))
                filterConfigCurrent.industry.selected.push(el.dataset.id);
        });

    filterConfigCurrent.asset.stockExchange = document.getElementById("stockExchangeCheck").checked;
    filterConfigCurrent.asset.stockOTC = document.getElementById("stockOTCCheck").checked;
    filterConfigCurrent.asset.basisOTC = document.getElementById("basisOTCCheck").checked;
    filterConfigCurrent.asset.priority = document.getElementById("priorityCheck").checked;
    filterConfigCurrent.asset.fund = document.getElementById("fundCheck").checked;
    filterConfigCurrent.asset.debtSecurity = document.getElementById("debtSecurityCheck").checked;
    filterConfigCurrent.asset.option = document.getElementById("optionCheck").checked;
    filterConfigCurrent.asset.commodity = document.getElementById("commodityCheck").checked;
    filterConfigCurrent.asset.future = document.getElementById("futureCheck").checked;

    filterConfigCurrent.intervalTime = getIntervalTime();

    filterConfigCurrent.sortProperty = document.getElementById("sortPropertyTextarea").value
    filterConfigCurrent.css = document.getElementById("cssInjectTextarea").value;

    filterConfigCurrent.code = editor.getValue();

    // console.log(`updateCurrentFilterConfig: title:${filterConfigCurrent.title}  id:${filterConfigCurrent.id} update`);
}

function saveFilterSetting() {
    // console.log(`saveFilterConfig:`);
    updateCurrentFilterConfig();
    Storage.add(filterConfigCurrent);
    // console.log(`saveFilterConfig: title:${filterConfigCurrent.title}  id:${filterConfigCurrent.id} saved`);
    updateFilterList();
}

document.getElementById('saveTemplateBtn')
    .addEventListener('click', () => {
        saveFilterSetting();
    })

//listener: create new filter config
document.getElementById('addNewFilter')
    .addEventListener('click', ev => {

        // console.log("addNewFilter:");
        if (document.querySelector("#newFilterTitle").checkValidity()) {
            console.log("addNewFilter: valid input");
            let title = $('#newFilterTitle').val();
            createFilterConfig(title)
        } else {
            ev.preventDefault();
            ev.stopPropagation();
            console.log("addNewFilter: invalid input");

        }
    })

//listener: select or remove filter config from dropdown list
document.querySelector('#filterListDropdown ul')
    .addEventListener('click', ev => {

        ev.preventDefault();

        const selectedItem = ev.target.closest('li > div.dropdown-item');

        if (selectedItem && ev.target.nodeName === 'DIV') { // select item
            showFilterConfig(selectedItem.dataset.id);
        } else if (selectedItem && ev.target.nodeName === 'I') { // remove item
            Storage.remove(selectedItem.dataset.id);
            updateFilterList();
        }
    })

function selectIndustryItem(id) {

    document.querySelector(`#industryDropdownMenu button[data-id='${id}']`).classList.toggle('active');
    (id === 'all') && updateIndustryCaption();
}

function deselectAllIndustryItem() {

    document.querySelectorAll(`#industryDropdownMenu button.active`)
        .forEach(el => {
            el.classList.remove('active');
        });
    document.getElementById('industryCaption').innerText = '';
}

function updateIndustryCaption() {

    document.getElementById('industryCaption').innerText =
        document.querySelector("#industryDropdownMenu button[data-id='all']")
            .classList.contains('active') ?
            'نمایش همه' : 'انتخاب شده از لیست';
}

document.querySelector('#industryDropdownMenu')
    .addEventListener('click', ev => {

        ev.preventDefault();
        ev.stopPropagation();
        let selectedItem = ev.target;

        if (selectedItem && selectedItem.nodeName === 'BUTTON') {
            selectIndustryItem(selectedItem.dataset.id);
        }

    })

function setIntervalTime(value) {
    document.getElementById("intervalTimeRange").value = value;
    document.getElementById("intervalTimeCaption").innerText = value + 's';
}

function getIntervalTime() {
    return document.getElementById("intervalTimeRange").value;
}

document.getElementById("intervalTimeRange")
    .oninput = function () {
    document.getElementById("intervalTimeCaption").innerText = this.value + 's';
}

document.getElementById('enableDateSwitch')
    .addEventListener('click', ev => {
        document.getElementById("datePicker").disabled = !document.getElementById("enableDateSwitch").checked;
    })

function getSelectedDate() {
    return document.getElementById("enableDateSwitch").checked ? // date enabled?
        dateToGregorian(document.getElementById("datePicker").value) : // return selected date
        false                                                         // return false value for last day
}

//filtering
document.getElementById('filterBtn')
    .addEventListener('click', () => {

        updateCurrentFilterConfig();
        let activeTableId, activeTable = document.querySelector('#tableContent > div.active');
        if (activeTable) {
            activeTableId = activeTable.dataset.id;

            let filterConfigClone = JSON.parse(JSON.stringify(filterConfigCurrent))
            if (!tfe[activeTableId]) {
                tfe[activeTableId] = new TableFilter(filterConfigClone, activeTable.id);
                tfe[activeTableId].initTable();
                tfe[activeTableId].initSortMenu();
            }

            tfe[activeTableId].filterConfig = filterConfigClone;
            tfe[activeTableId].date = getSelectedDate();
            tfe[activeTableId].prepareFilter();
            tfe[activeTableId].startFiltering();
            tfe[activeTableId].renderSortMenuItem();

        } else {
            $('#filterErrorToast').toast('show');
        }

    })

function createNewTable(title) {

    let lastTab = document.querySelector('#tableTab li:last-of-type a');
    let id = lastTab ? lastTab.dataset.id : 0;
    id++;

    document.querySelector("#createTableModal")
        .insertAdjacentHTML('beforebegin', `
                    <li class="nav-item">
                        <a class="nav-link p-2" href="#table${id}" data-id="${id}">
                            <span>${title}</span>
                            <button class="btn btn-close btn-close-sm ms-1"></button>
                        </a>
                    </li>`
        );

    //table Content
    document.querySelector("#tableContent")
        .insertAdjacentHTML('beforeend', `
                    <div id="table${id}" class="tab-pane fade" data-id="${id}"></div>`
        );

    $(`#tableTab a[href="#table${id}"]`).tab('show');
}

function deleteTable(id) {
    if (tfe[id]) {
        tfe[id].stopFiltering()
        delete tfe[id];
    }

    document.querySelector(`#tableContent div[data-id='${id}']`).remove();
    document.querySelector(`#tableTab li a[data-id='${id}']`).remove();
}

// listener: show create Table Modal
document.getElementById('createTableModal')
    .addEventListener('click', () => {
        document.getElementById('getTitleModalTitle').value = filterConfigCurrent.title;
        $('#getTitleModal').modal('show');
    })

// listener: create new Table (from modal)
document.getElementById('createNewTable')
    .addEventListener('click', () => {
        let title = document.getElementById('getTitleModalTitle').value;
        createNewTable(title);
        $('#getTitleModal').modal('hide');
    })

//listener: select table or remove table
document.getElementById('tableTab')
    .addEventListener('click', ev => {
        const selectedTab = ev.target.closest('#tableTab li a.nav-link');
        if (selectedTab && ev.target.nodeName === 'BUTTON' && ev.target.classList.contains('btn-close')) {  //delete tab
            deleteTable(selectedTab.dataset.id);
        } else if (selectedTab && (ev.target.nodeName === 'A' || ev.target.nodeName === 'SPAN')) { //select tab
            $(selectedTab).tab('show');
        }
    })

document.getElementById('filterSettingSwitch')
    .addEventListener('click', ev => {
        $('#filterSettingCard').slideToggle("slow");
    })

document.getElementById('notifySwitch')
    .addEventListener('click', ev => {
        //todo
    })




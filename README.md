<div dir=rtl align=right>

ابزاری برای کوئری نویسی، فیلتر کردن و نمایش اطلاعات نمادهای بورس تهران الهام گرفته از ابزار [فیلترنویسی سایت بورس تهران](http://www.tsetmc.com/Loader.aspx?ParTree=15131F) با امکانات بیشتر وعملکرد بهتر.


* اجرای کوئری
  در تاریخ گذشته.
* امکان مرتب
  سازی بر اساس Property تعریف شده یا Field.
* فیلتر
  نویسی ساختاریافته شامل فانکشن‌‌های Init() ،compute() ، ()filter و Global Variable برای کش کردن دیتا.
* Multi Tab


![carmain-cover](doc/Screenshot.gif)

دریافت و راه‌اندازی:

</div>

```bash
git clone git@gitlab.com:AMD97/tse-filter-app.git

yarn install

yarn package
```
